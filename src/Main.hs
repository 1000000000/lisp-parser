module Main where

import Control.Applicative

import Data.Char
import Data.Maybe

import Text.ParserCombinators.ReadP

data Operator
  = Plus
  | Minus
  | Times
  | Divide
  | Mod
  | Rem
  | Equal
  | NotEqual
  | LessThan
  | GreaterThan
  | LessThanOrEqual
  | GreaterThanOrEqual
  | Max
  | Min
  | And
  | Or
  | Not
  | ListOp
  | First
  deriving Show

data Atom
  = NumberAtom Integer
  | OperatorAtom Operator
  | CustomAtom String
  deriving Show

data LispAST
  = List [LispAST]
  | Atom Atom
  deriving Show

parseNumber :: ReadP Integer
parseNumber = readS_to_P reads

parseOperator :: ReadP Operator
parseOperator = choice
  [ Plus               <$ string "+"
  , Minus              <$ string "-"
  , Times              <$ string "*"
  , Divide             <$ string "/"
  , Mod                <$ string "mod"
  , Rem                <$ string "rem"
  , Equal              <$ string "="
  , NotEqual           <$ string "/="
  , LessThan           <$ string "<"
  , GreaterThan        <$ string ">"
  , LessThanOrEqual    <$ string "<="
  , GreaterThanOrEqual <$ string ">="
  , Max                <$ string "max"
  , Min                <$ string "min"
  , And                <$ string "and"
  , Or                 <$ string "or"
  , Not                <$ string "not"
  , ListOp             <$ string "list"
  , First              <$ string "first"
  ]
  

-- Custom can be any string that starts with an
-- alphabetic character and is then followed by alphanumeric characters
parseCustom :: ReadP String
parseCustom = liftA2 (:) (satisfy isAlpha) (munch isAlphaNum)

parseAtom :: ReadP Atom
parseAtom = NumberAtom <$> parseNumber <|> OperatorAtom <$> parseOperator <|> CustomAtom <$> parseCustom

parseList :: ReadP [LispAST]
parseList = between (char '(' <* skipSpaces) (skipSpaces *> char ')') $ sepBy parseLisp $ munch1 isSpace

parseLisp :: ReadP LispAST
parseLisp = Atom <$> parseAtom <|> List <$> parseList

parseExpression :: ReadP LispAST
parseExpression = skipSpaces *> parseLisp <* skipSpaces <* eof

runLispParser :: String -> Maybe LispAST
runLispParser = fmap fst . listToMaybe . readP_to_S parseExpression

main :: IO ()
main = lines <$> getContents >>= mapM_ (maybe (putStrLn "Parse error") print . runLispParser)
